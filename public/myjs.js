//variables must be declared
var name = "fred"; //value type = string
var age = 45; //value type = number
var married = true; //value type = boolean - either true or false
var address;//value type = undefined - no information on it

console.info("name = " + name);
console.info("age = " + age)
console.info("married = " + married);

// When using only one = sign, it is for assignment of variables
// Using 2 = signs, ie age == 50, it is a comparison
// <= less than or equal
// ! is a not, hence != not equals to
// >= more than or equal

if (age == 50) {
    console.log("Have you applied for your Pioneer card yet?")
} else {
    console.log("You should give up your seat to the elderly")
}

var i = 0; //Initialisation of variables - ALWAYS initialise variables
document.write("<ul>");
while (i < 5) {
    document.write("<li>Item " + i + "</li>");
    i++;
}
document.write("</ul");

for (i =0; i < 5; i++) {
    document.write("<li>****Item " + i + "</li>");
}
document.write("</ul");