//load express
var express = require("express")

//create an application
var app = express();

//define document root as /public.
// static resources will be served from here
app.use(
    express.static(__dirname + "/public")
);

//start server to listen on port 3000
app.listen(3000, function(){
    console.info("Application is listening on port 3000");

});